//
//  TestBass3AppDelegate.m
//  TestBass3
//
//  Created by admin on 2/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TestBass3AppDelegate.h"

#include "bass.h"
#include "bass_fx.h"
#include "bassmix.h"

#import <AudioToolbox/AudioToolbox.h>

@implementation TestBass3AppDelegate

@synthesize window;


#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    
    // Override point for customization after application launch.
    
    [self.window makeKeyAndVisible];
	
	[[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
	
    return YES;
}

- (IBAction)normal{
	
	BASS_ChannelSetAttribute(stream, BASS_ATTRIB_TEMPO_FREQ, 44100);
	
	BASS_ChannelSetAttribute(stream, BASS_ATTRIB_TEMPO_PITCH, +0);
	
	NSLog(@"music load error code: %d", BASS_ErrorGetCode());
}

- (IBAction)harfSpeed{
		
	BASS_ChannelSetAttribute(stream, BASS_ATTRIB_TEMPO_FREQ, 22500);
	
	BASS_ChannelSetAttribute(stream, BASS_ATTRIB_TEMPO_PITCH, +12);
	
	NSLog(@"music load error code: %d", BASS_ErrorGetCode());
	
}

- (IBAction)doubleSpeed{
	
	BASS_ChannelSetAttribute(stream, BASS_ATTRIB_TEMPO_FREQ, 88200);
		
	BASS_ChannelSetAttribute(stream, BASS_ATTRIB_TEMPO_PITCH, -11);
	
	NSLog(@"music load error code: %d", BASS_ErrorGetCode());
	
}

- (IBAction)_75Speed{

	BASS_ChannelSetAttribute(stream, BASS_ATTRIB_TEMPO_FREQ, 33075);
	
	BASS_ChannelSetAttribute(stream, BASS_ATTRIB_TEMPO_PITCH, +6);
}

- (IBAction)_1_5speed{
	
	BASS_ChannelSetAttribute(stream, BASS_ATTRIB_TEMPO_FREQ, 66150);
	
	BASS_ChannelSetAttribute(stream, BASS_ATTRIB_TEMPO_PITCH, -6);

	NSLog(@"music load error code: %d", BASS_ErrorGetCode());
}

- (IBAction)selectResource:(id)sender{
	
	BASS_ChannelStop(stream);
	UIButton * button = sender;
	switch (button.tag) {
		case 0:
			resourceName = @"Conversation";
			resourceType = @"m4b";
			break;
		case 1:
			resourceName = @"01";
			resourceType = @"aiff";
			break;
		case 2:
			resourceName = @"Dress";
			resourceType = @"m4a";
			break;
		case 3:
			resourceName = @"07";
			resourceType = @"wav";
			break;
		case 4:
			resourceName = @"范文";
			resourceType = @"MP3";
			break;
		case 5:
			resourceName = @"普特英语";
			resourceType = @"mp3";
			break;

		default:
			break;
	}
	
	BASS_ChannelStop(stream);
	BASS_Init(-1, 44100, 0, 0, 0); // initialize output device
	
	NSString *respath=[[NSBundle mainBundle] pathForResource:resourceName ofType:resourceType]; // get path of audio file
	stream=BASS_StreamCreateFile(0, [respath UTF8String], 0, 0, BASS_STREAM_DECODE); // create a stream from it
	
	
	HSTREAM s2 = BASS_Mixer_StreamCreate(44100, 2, BASS_STREAM_DECODE);
	
	BASS_Mixer_StreamAddChannel(s2, stream, BASS_STREAM_DECODE);
	
	stream = BASS_FX_TempoCreate(stream, BASS_FX_FREESOURCE);
	
	NSLog(@"music load error code: %d, %d", BASS_ErrorGetCode(), BASS_ERROR_HANDLE);
	
	BASS_ChannelPlay(stream, FALSE);
	
	NSLog(@"music load error code: %d", BASS_ErrorGetCode());
}


- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}


- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}

- (void)playPause {
	if (BASS_ChannelIsActive(stream) == BASS_ACTIVE_PLAYING) {
		BASS_Pause();
	} else {
		BASS_Start();
	}
}


- (void)dealloc {
    [window release];
    [super dealloc];
}


@end
