//
//  TestBass3AppDelegate.h
//  TestBass3
//
//  Created by admin on 2/16/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "bass.h"
#import <AVFoundation/AVFoundation.h>

@interface TestBass3AppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
	HSTREAM stream;
	NSString * resourceName;
	NSString * resourceType;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;

- (IBAction)normal;
- (IBAction)harfSpeed;
- (IBAction)_75Speed;
- (IBAction)doubleSpeed;
- (IBAction)_1_5speed;

- (IBAction)selectResource:(id)sender;

- (void)playPause;

@end

